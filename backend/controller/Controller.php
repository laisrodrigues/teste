<?php

include_once '../util/Input.php';
include_once 'VendedorController.php';


$preclass = Input::request('class');
$class = $preclass . 'Controller';
$method = Input::request('method');
$parameters = Input::request('parameters');

$obj = new $class();
$res = $obj->$method();
echo $res;
