<?php

class Input {

    static function request($request, $resEmpty = '') {
        $novo = (isset($_REQUEST[$request])) ? strip_tags(addslashes(trim(preg_replace('/ +/', ' ', $_REQUEST[$request])))) : $resEmpty;
        return $novo;
    }

}
