<?php

include_once 'Conexao.php';
include_once 'Vendedor.php';

class VendedorDAO extends Conexao{

    public $con = NULL;

    public function __construct() {
        $conexao = new Conexao();
        $this->con = $conexao->conecta();
    }

    function inserir(Vendedor $vendedor) {
        try {

            $con = Conexao::conecta();

            $stmt = $con->prepare('INSERT INTO vendedor (id, nome, vendedor_1, vendedor_2, codigo) '
                    . 'VALUES(:id, :nome, :vendedor_1, :vendedor_2, :codigo)');

            $stmt->execute(array(
                ':id' => '',
                ':nome' => $vendedor->getNome(),
                ':vendedor_1' => $vendedor->getVendedor_1(),
                ':vendedor_2' => $vendedor->getVendedor_2(),
                ':codigo' => $vendedor->getCodigo(),
            ));

            if ($stmt->lastInsertId() > 0) {
                echo json_encode(1);
            } else {
                echo $json_encode(0);
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    function pesquisar() {
        try {
            
            $sql = "SELECT * FROM vendedor";

            $stmte = $this->con->query($sql);
            $stmte->execute();
            
            return json_encode($stmte->fetchAll(PDO::FETCH_OBJ));
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

}
