<?php

class Conexao {

    public $servidor = 'localhost';
    public $usuario = 'root';
    public $senha = '';
    public $nomebanco = 'teste';

    public function conecta() {
        $conexao = new PDO("mysql:host=" . $this->servidor . ";dbname=" . $this->nomebanco . ";", "" . $this->usuario . "", "" . $this->senha . "", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::ATTR_PERSISTENT => true));
        $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conexao;
        
    }

}
