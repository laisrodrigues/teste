<?php

class Vendedor {
    
    private $id;
    private $nome;
    private $vendedor_1;
    private $vendedor_2;
    private $codigo;
    
    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getVendedor_1() {
        return $this->vendedor_1;
    }

    function getVendedor_2() {
        return $this->vendedor_2;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setVendedor_1($vendedor_1) {
        $this->vendedor_1 = $vendedor_1;
    }

    function setVendedor_2($vendedor_2) {
        $this->vendedor_2 = $vendedor_2;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }


    
}
