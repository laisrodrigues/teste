<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title></title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="stylesheet" href="assets/css/materialize.min.css" />
    </head>
    <body>

        <div class="container">
            <nav>
                <div class="nav-wrapper">
                    <a href="#!" class="brand-logo center">Logo</a>
                    <ul class="left hide-on-med-and-down">
                        <li><a href="sass.html">Sass</a></li>
                        <li><a href="badges.html">Components</a></li>
                        <li class="active"><a href="collapsible.html">JavaScript</a></li>
                    </ul>
                </div>
            </nav>
            
            <div id="gridVendedor"></div>



        </div>

        <script src="assets/js/jquery-3.4.1..js"></script>
        <script src="assets/js/vendedor.js"></script>
        <script>
            var vendedor = new Vendedor();
            $(document).ready(function () {
                vendedor.pesquisar(1);
            });
        </script>

    </body>
</html>





