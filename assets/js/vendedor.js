var Vendedor = {};

Vendedor = function () {
    var self = this;

    self.pesquisar = function () {

        $.ajax({
            type: 'post',
            url: 'backend/controller/Controller.php',
            data: {class: 'Vendedor', method: 'pesquisar', parameters: ''},
            dataType: 'json',
            success: function (ret) {
                $('.retornoMsg').html('');
                if (ret == 0) {
                    grid = "<div class='alert alert-warning' role='alert'>Nenhum vendedor encontrado</div>";
                    $('.paginacao').html('');
                    $('#gridMaterial').html(grid);
                } else {
                    var grid = '<table class="table table-bordered table-condensed table-striped">'
                            + '  <tr>'
                            + '      <th style="width:3%;">Id</th>'
                            + '      <th>Nome</th>'
                            + '      <th style="width:3%;">Vendedor 1</th>'
                            + '      <th style="width:3%;">Vendedor 2</th>'
                            + '      <th style="width:3%;">Codigo</th>'
                            + '      <th style="width:3%;">Editar</th>'
                            + '      <th style="width:3%;">Excluir</th>'
                            + '  </tr>';

                    $.each(ret, function (chave, valor) {
                        grid += '<tr>';
                        grid += '<td>' + valor['id'] + '</td>';
                        grid += '<td>' + valor['nome'] + '</td>';
                        grid += '<td>' + valor['vendedor_1'] + '</td>';
                        grid += '<td>' + valor['vendedor_2'] + '</td>';
                        grid += '<td>' + valor['codigo'] + '</td>';
                        grid += '<td>' + valor['editar'] + '</td>';
                        grid += '<td>' + valor['excluir'] + '</td>';
                        grid += '</tr>';
                    });
                    $('#gridVendedor').html(grid);
                }
            }
        });

    };

}


